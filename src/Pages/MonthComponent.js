import React, { useState ,useEffect } from 'react';
import {useParams} from "react-router-dom";
import {useDispatch} from "react-redux";
import {setMonthYear} from "../Redux/reducers/appointmentReducer";
import {months} from "../Library/helper";
import DayComponent from "../Components/DayComponent";
import {CalenderHeader} from "../Style/style";

const MonthComponent = () => {
    const { year:urlYear, month:urlMonth } = useParams();
    const [ days, setDays ]  = useState(0)
    const dispatch = useDispatch();
    const appointments = []
    let counter = 1;

    const dispatchMonthsAndYears = async () => {
        await dispatch(setMonthYear({"month": urlMonth, "year": urlYear}));
    }

    const getDays = (days) => {
        while ( counter <= days) {
            appointments.push(<DayComponent key={counter}>{counter}</DayComponent>)
            counter++;
        }
        return appointments
    }

    useEffect(() => {
        dispatchMonthsAndYears()
            .then(()=> {
                setDays(months[urlMonth-1].days);
            });
    },[urlYear, urlMonth]);

    return(
        <div className='container'>
            <CalenderHeader>
                <h3>{urlYear}/{months[urlMonth-1].name}</h3>
            </CalenderHeader>
            <div className="d-flex flex-row bd-highlight flex-wrap">
                {getDays(days)}
            </div>
        </div>
    );
}

export default MonthComponent;