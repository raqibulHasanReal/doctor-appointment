const date = new Date();

export const months = [
    { 'name':'January', 'days': 31},
    { 'name':'February', 'days': 28},
    { 'name':'March', 'days': 31},
    { 'name':'April', 'days': 30},
    { 'name':'May', 'days': 31},
    { 'name':'June', 'days': 30},
    { 'name':'July', 'days': 31},
    { 'name':'August', 'days': 31},
    { 'name':'September', 'days': 30},
    { 'name':'October', 'days': 31},
    { 'name':'November', 'days': 30},
    { 'name':'December', 'days': 31}];

export const currentMonth = () => {
    return date.getMonth()+1;
}

export const currentYear = () => {
    return date.getFullYear();
}

export const formatAMPM = (hours, minutes) => {
    let ampm = hours >= 12 ? 'pm' : 'am';
    hours = hours % 12;
    hours = hours ? hours : 12;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    return hours + ':' + minutes + ' ' + ampm;
}