import styled from "styled-components";

export const DayWrapper = styled.div`
  display: block;
  position: relative;
  border: 1px solid #6c757d;
  padding: 12px;
  margin: 14px 14px 0 0;
  height: 135px;
  width: 170px;
  overflow: hidden;
`;

export const Day = styled.div`
  height: 100%;
  width: 170px;
  overflow-y: scroll;
`;

export const CalenderDate = styled.div`
  font-size: 32px;
  font-weight: 600;
  color: #6c757d;
`;

export const AppointmentsDiv = styled.div`
  text-align: right;
  display: flex;
  flex-flow: column;
  row-gap: 6px;
  margin-right: 12px;
`;

export const AppointmentButton = styled.button`
  display: inline-block;
  font-weight: 400;
  line-height: 1.5;
  text-align: center;
  text-decoration: none;
  vertical-align: middle;
  cursor: pointer;
  -webkit-user-select: none;
  user-select: none;
  color: #fff;
  background-color: #6c757d;
  padding: .1rem .5rem;
  font-size: .875rem;
  border-radius: .2rem;
  border: 1px solid transparent;
  transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
`;

export const CalenderHeader = styled.div`
  text-align: center;
  margin: 1rem!important;
  color: #6c757d;
`;

export const PageNotFound = styled.div`
  text-align: center;
  padding: 32px;
  margin: 32px;
  color: #6c757d;
  font-weight: 600;
`;

export const Label = styled.div`
flex: 1
`;

export const Details = styled.div`
flex: 2
`;
