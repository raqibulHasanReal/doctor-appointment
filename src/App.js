import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import PageNotFound from '../src/Pages/PageNotFound'
import { BrowserRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Header from "./Components/Header";
import {currentMonth, currentYear} from "./Library/helper";
import MonthComponent from "./Pages/MonthComponent";

function App() {

    const rootRoute = `/year/${currentYear()}/month/${currentMonth()}`;
    const renderComponent = (props) => {
        const month = props.match.params.month;
        const year = props.match.params.year;
        if(Number(month) && Number(month) <= 12 && Number(month) > 0 && Number(year)) {
            return <MonthComponent/>
        }
        return <PageNotFound />
    }
    return (
    <Router>
        <Header/>
        <Switch>
            <Route exact path="/">
                <Redirect to={rootRoute} />
            </Route>
            <Route path='/year/:year/month/:month' exact render= {(props) => { return renderComponent(props)}}/>
            <Route path='*' exact component={PageNotFound} />
        </Switch>
    </Router>
  );
}

export default App;
