import React from 'react';
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import {Link} from "react-router-dom";
import {months} from "../Library/helper";
import {useSelector} from "react-redux";
import {monthAndYear} from "../Redux/reducers/appointmentReducer";
import CreateAppointment from "./CreateAppointment";

const Header = () => {
    const getMonths = () => {
        const monthList = [];
        months.map((month, index)=> {
            monthList.push( <NavDropdown.Item as={Link} key={month.name} to={`/year/${useSelector(monthAndYear).year}/month/${index+1}`}>{month.name}</NavDropdown.Item>)
        })
        return monthList;
    }
    const getYears = () => {
        let years = [];
        for (let i=2019; i<2022; i++) {
            years.unshift(<NavDropdown.Item as={Link} key={i} to={`/year/${i}/month/${useSelector(monthAndYear).month}`}>{i}</NavDropdown.Item>);
        }
        return years;
    }

    return(
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand> <Link className="text-decoration-none text-secondary fw-bolder" to="/">Doctor Appointment</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <NavDropdown title="Year" id="basic-nav-dropdown">
                            {getYears()}
                        </NavDropdown>

                        <NavDropdown title="Month" id="basic-nav-dropdown">
                            {getMonths()}
                        </NavDropdown>
                    </Nav>
                    <div>
                        <CreateAppointment showDetails={false}/>
                    </div>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default Header;
