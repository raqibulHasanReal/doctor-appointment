import React from 'react';
import { useForm } from "react-hook-form";
import { storeAppointment } from "../Redux/reducers/appointmentReducer";
import { useDispatch } from "react-redux";


function AppointmentInputForm(props) {
    const { register, handleSubmit, reset, formState: { errors } } = useForm();
    const dispatch = useDispatch();

    const dispatchAppointment = async (appointment) => {
        dispatch(storeAppointment(appointment));
    }

    const onSubmit = data => {
        let time = data.time.replace(":", ".");
        let [year, month, date] = data.date.split("-")
        let appointment = {"name": data.name, "age": Number(data.age), "gender": data.gender, "time": Number(time),"date": Number(date), "month": Number(month), "year":  Number(year)}
        dispatchAppointment(appointment)
            .then(() => {
                reset();
                props.modalOff()
            })
    }

    const Select = React.forwardRef(({ onChange, onBlur, name, label }, ref) => (
        <div className="mb-2">
            <label>{label}</label>
            <select className="form-control" required={true} name={name} ref={ref} onChange={onChange} onBlur={onBlur}>
                <option hidden value="" >Gender</option>
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </div>
    ));

    return (
        <>
            <form onSubmit={handleSubmit(onSubmit)}>
                <h4> Appointment Form </h4>
                <div className="form-group mb-2">
                    <label htmlFor="username">Name</label>
                    <input className="form-control" required={true} placeholder="name" {...register("name")} />
                </div>

                <Select label="Gender" {...register("gender")} />

                <div className="form-group mb-2">
                    <label htmlFor="username">Age</label>
                    <input className="form-control" required={true} placeholder="age" {...register("age")} />
                </div>

                <div className="form-group mb-2">
                    <label htmlFor="username">Date</label>
                    <input className="form-control" required={true} placeholder="date" max="2021-12-31" min="2019-01-01" type="date" {...register("date")} />
                </div>

                <div className="form-group mb-2">
                    <label htmlFor="username">Time</label>
                    <input className="form-control" required={true} placeholder="time" type="time" {...register("time")} />
                </div>

                <div className="float-end mt-3">
                    <input className="btn btn-primary btn-sm px-5" type="submit" />
                </div>
            </form>
        </>
    );
}

export default AppointmentInputForm