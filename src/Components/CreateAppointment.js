import React from "react";
import {AppointmentButton} from "../Style/style";
import WithModal from "./HOC/withModal";

const CreateAppointment  = (props) => {
    const openModal = () => {
        props.clickHandler();
    }
    return(
        <AppointmentButton onClick={openModal}>
            create appointment
        </AppointmentButton>
    );
}

export default WithModal(CreateAppointment);