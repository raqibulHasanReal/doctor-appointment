import React from 'react';
import {formatAMPM, months} from "../Library/helper";
import {Details, Label} from "../Style/style";

function AppointmentDetails(props) {
    const { name, age, gender, time, month, year, date } = props.appointment;
    const [hours, minutes=0] = time.toString().split('.');
    let formatTime = formatAMPM(Number(hours), Number(minutes));
    return (<>
            <h5>Appointment Details</h5>
            <ul className="list-group">
                <li className="list-group-item d-flex">
                    <Label>Name:</Label>
                    <Details>{name}</Details>
                </li>
                <li className="list-group-item d-flex">
                    <Label>Age:</Label>
                    <Details>{age}</Details>
                </li>
                <li className="list-group-item d-flex">
                    <Label>Gender:</Label>
                    <Details>{gender}</Details>
                </li>
                <li className="list-group-item d-flex">
                    <Label>Date:</Label>
                    <Details>{date}/{months[month-1].name}/{year}</Details>
                </li>
                <li className="list-group-item d-flex">
                    <Label>Time:</Label>
                    <Details>{formatTime}</Details>
                </li>
            </ul>
    </>
    );
}

export default AppointmentDetails;