import React, {useState} from 'react';
import {Modal} from "react-bootstrap";
import AppointmentInputForm from "../AppointmentInputForm";
import AppointmentDetails from "../AppointmnetDetails";

function WithModal(ParentComponent) {
    function NewComponent(props) {
        const { appointment, showDetails: showAppointmentDetails } = props;
        const [show, setShow] = useState(false);
        const handleClose = () => {
            setShow(false)
        };
        const openModal = () => {
            setShow(true);
        }
        return (<>
                <ParentComponent clickHandler = {openModal} appointment={appointment}/>
                <Modal show={show} onHide={handleClose} animation={false}>
                    <Modal.Body>
                        {
                            showAppointmentDetails ?
                                <AppointmentDetails appointment={appointment}/> :
                                <AppointmentInputForm modalOff={handleClose}/>
                        }
                    </Modal.Body>
                </Modal>
            </>
        );
    }
    return NewComponent;
}
export default WithModal