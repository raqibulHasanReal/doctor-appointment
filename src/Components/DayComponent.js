import React, {useEffect, useState} from 'react';
import {appointmentList} from "../Redux/reducers/appointmentReducer";
import {useSelector} from "react-redux";
import {AppointmentsDiv, CalenderDate, Day, DayWrapper} from "../Style/style";
import Appointment from "./Appointment";

const DayComponent = (props) => {
    const appointments = useSelector(appointmentList);
    const [appointmentLiHtml, setAppointmentLiHtml] = useState([]);

    const toDaysAppointment = [];
    const list = [];

    const getToDaysAppointment = () => {
        appointmentLiHtml.map((item, index)=> {
            list.push(<div key={index}><Appointment showDetails={true} appointment={item}/></div>);
        })
        return list;
    }

    const setAppointments = () => {
        appointments.map((appointment) => {
            if (props.children === appointment.date){
                toDaysAppointment.push(appointment);
            }
        });
        toDaysAppointment.sort(function(a, b) {
            return a.time - b.time;
        });
        setAppointmentLiHtml(toDaysAppointment)
    }

    useEffect(() => {
        setAppointments();
    },[appointments]);

    return(
        <DayWrapper>
            <Day>
                <CalenderDate >
                    {props.children}
                </CalenderDate>
                <AppointmentsDiv>
                    { !!appointmentLiHtml.length && getToDaysAppointment()}
                </AppointmentsDiv>
            </Day>
        </DayWrapper>
    );
}

export default DayComponent;