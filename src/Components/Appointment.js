import React from "react";
import {AppointmentButton} from "../Style/style";
import WithModal from "./HOC/withModal";

const Appointment  = (props) => {
    const {appointment} = props;
    const openModal = () => {
       props.clickHandler();
    }
    return(
        <AppointmentButton onClick={openModal}>
            {appointment.name}
        </AppointmentButton>
    );
}

export default WithModal(Appointment);