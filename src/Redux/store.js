import { configureStore } from '@reduxjs/toolkit';
import appointmentReducer from './reducers/appointmentReducer';

export const store = configureStore({
    reducer: {
        appointmentDetails: appointmentReducer
    },
});