import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';

const date = new Date();
function getAppointments(month, year) {
    const appointments = localStorage.getItem('appointments') ? JSON.parse(localStorage.getItem('appointments')) : [];
    const currentMonthAppointment = []
    appointments.map((item) => {
        if ( item.month == month && item.year == year ) {
            currentMonthAppointment.push(item);
        }
    });
    return currentMonthAppointment;
}
const initialState = {
    monthAndYear: {"month": date.getMonth()+1, "year": date.getFullYear()},
    appointments: getAppointments(date.getMonth()+1, date.getFullYear())
};
export const storeAppointment = createAsyncThunk(
    'appointment/store',
    async (appointment, { rejectWithValue }) => {
        try {
            const appointments = localStorage.getItem('appointments') ? JSON.parse(localStorage.getItem('appointments')) : [];
            appointments.push(appointment);
            localStorage.setItem('appointments', JSON.stringify(appointments))
            return appointment;
        } catch (e) {
            console.log(e)
        }
    }
);
export const appointmentReducer = createSlice({
    name: 'appointment',
    initialState,
    reducers: {
        setMonthYear: (state, action) => {
            state.appointments = getAppointments(action.payload.month, action.payload.year)
            state.monthAndYear = action.payload
        }
    },
    extraReducers: (builder) => {
        builder
            .addCase(storeAppointment.rejected, (state, action) => {
                console.log('rejected')
            })
            .addCase(storeAppointment.fulfilled, (state, action) => {
                state.appointments = getAppointments(state.monthAndYear.month, state.monthAndYear.year)
            })
    },
})
export const { setMonthYear } = appointmentReducer.actions;
export const monthAndYear = (state) => state.appointmentDetails.monthAndYear;
export const appointmentList = (state) => state.appointmentDetails.appointments;
export default appointmentReducer.reducer