Installation and Setup Instructions

Example:
Clone down this repository. You will need node and npm installed globally on your machine.

<hr/>
Installation:

npm install

<hr/>
To Start Server:

npm start


<hr/>
To Visit App:

localhost:3000
<hr/>

Project Screen Shot(s) <br>
Example:

![Alt text](./public/doctorAppointment.png?raw=true?width=600 "Title")
